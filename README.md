# soal-shift-sisop-modul-2-I08-2022

## REPORT GROUP I08 
| Nama		 		 | NRP		  | 
| :---         		 |     :---:      | 
| Deon Fitra Tama    | 5025201115     | 
| M. Akmal Riswanda  | 5025201143     | 
| Venia Sollery A H. | 5025201161	  |

## REPORT NO.1 
### Question A 
when the program first runs. The program will download the characters file and weapons file from the link below, then the program will extract both files the. The file will be used as a database for gacha items characters and weapons. Then a folder will be created with the name "gacha_gacha" as working directory. All gacha results will be in those folders.

In this case of question in program we can make function to refers a link online and dowload it the put into new directory. 

```c
void folderdownload(){
	pid_t child_id;
 	child_id = fork();
  	if (child_id < 0) {
		exit(EXIT_FAILURE);
	}
if (child_id == 0) {
		unzipdownloader();
	} 
  	else {
    	char *argv1[][10] = {
			{ "wget","-q","--no-check-certificate","https://drive.google.com/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download", "-O","weapon.zip",NULL},
			{ "wget","-q","--no-check-certificate","https://drive.google.com/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download", "-O","characters.zip",NULL},
		};
		for(int i=0;i<2;i++){
			if(fork()==0) continue;
			execv("/bin/wget", argv1[i]);
		}
		sleep(15);i
  	}
}
```

In the other hand, we should make other fuction to implement unzip a file after we download it. 

```c
void unzipdownloader(){
	pid_t child_id;
	child_id = fork();
	
	if (child_id < 0) {
		exit(EXIT_FAILURE);
	}
if (child_id == 0) {
	} 
	else {
		char *argv3[][10]= {
			{"unzip","-j","characters.zip","-d","./characters",NULL},
			{"unzip","-j","weapon.zip","-d","./weapon",NULL}
		};
	for(int i=0; i<3;i++){
			if(fork()==0) continue;
			sleep(10);
			execv("/bin/unzip", argv3[i]); 
		}
	}
}
```

Lastly to import the file have been downloaded we make the folder name `gacha_gacha`, `characters`, and `weapon` to implement made a fuction to generate the directory. 

```c
void directorygenerator(){
	pid_t child_id;
  	child_id = fork();
  	if (child_id < 0) {
    	exit(EXIT_FAILURE);
  	}
	if (child_id != 0) {
    	folderdownload();
  	}
  	else {
		char *argv[][6] = {{"mkdir","gacha_gacha",NULL},
			{"mkdir","characters",NULL},
			{"mkdir","weapon",NULL}
		};

		for(int anjg=0; anjg<3;anjg++){
		    if(fork()==0) continue;
		    sleep(10);
		    execv("/bin/mkdir", argv[anjg]); 
		}
  	}
}
```
### Question B
Mas Refadi wants every time gacha, item characters and item weapons will always be alternately retrieved data from the database. Then for each time the gacha-number an even value will be carried out by gacha item weapons, if the value is odd then the itemcharacters. Then for every time the gacha-sum is mod 10, it will be made a new file (.txt) and the next gacha output will be in the file the new. And every time the gacha-sum is mod 90, a will be created new folder and the next file (.txt) will be in the new folder. So for each folder, there will be 9 files (.txt) which contains 10 gacha results. And because this is a gacha simulation, the result of gacha in the .txt file isRANDOM / RANDOM and each file (.txt) its contents will be DIFFERENT

To solve we made a fuction to gacha the to objects which are Weapons and Characters. The fuction code written as below:

```c
void gachawangy(int money, int gachaxx){
	char temp[10];
	char hero[70];
	int rndgnet = (time(0)+rand())%128;
	srand(gachaxx);		
	strcpy(hero, "./weapon/");
	strcat(hero, weapon[rndgnet]);
	sprintf(tempresult[gachaxx%10], "%d", gachaxx+1);
	strcat(tempresult[gachaxx%10], "_Waepon_");
	strcat(tempresult[gachaxx%10], rarity(hero));
	strcat(tempresult[gachaxx%10], "_");
	strcat(tempresult[gachaxx%10], getname(hero));
	strcat(tempresult[gachaxx%10], "_");
	sprintf(temp, "%d", money);
	strcat(tempresult[gachaxx%10], temp);
	printf(">>> %s\n", tempresult[gachaxx%10]);
}

void gachawangychr(int money, int gachaxx){
	char temp[10];
	char name[50];
	char hero[50];
	int rndgnet = (time(0)+rand())%47;
	srand(gachaxx);
	strcpy(hero, "./characters/");
	strcat(hero, characters[rndgnet]);
	sprintf(tempresult[gachaxx%10], "%d", gachaxx+1);
	strcat(tempresult[gachaxx%10], "_characters_");
	strcat(tempresult[gachaxx%10], raritychr(hero));
	strcat(tempresult[gachaxx%10], "_");
	strcat(tempresult[gachaxx%10], getnamechr(hero));
	strcat(tempresult[gachaxx%10], "_");
	sprintf(temp, "%d", money);
	strcat(tempresult[gachaxx%10], temp);
	printf(">>> %s\n", tempresult[gachaxx%10]);
}

```

### Question C
The naming format for each file (.txt) is {Hh:Mm:Ss}_gacha_{number-gacha}, for example 04:44:12_gacha_120.txt, and the naming format for each folder is total_gacha_{sum-gacha}, eg total_gacha_270. And for each file (.txt) will have a difference in naming the output time of 1 second.

To solve that we could make a function namely store to store result of each gacha into a txt file. 

```c
void store(int gacha, int foldercount){
	printf(">>aa");
	time_t t;
    t = time(NULL);
    struct tm tm;
    tm = *localtime(&t);
	char titletime[20];
	char tmp[15];
	sprintf(titletime, "%02d", (tm.tm_hour+14)%24);
	strcat(titletime, ":");
	sprintf(tmp, "%02d", tm.tm_min);
	strcat(titletime, tmp);
	strcat(titletime, ":");
	sprintf(tmp, "%02d", tm.tm_sec);
	strcat(titletime, tmp);
	strcat(titletime, "_gacha_");
	sprintf(tmp, "%d", gacha);
	strcat(titletime, tmp);
	strcpy(txtname[foldercount],"./gacha_gacha/");
	strcat(txtname[foldercount],titletime);
    
    FILE * fPtr;
    fPtr = fopen(titletime, "w");
    if(fPtr == NULL){
        printf("Unable to create file.\n");
    }
    else{
    	for(int i = 0 ; i <10 ; i++){
    		fputs(tempresult[i],fPtr);
    		fputs("\n",fPtr);
    	}
		
    	fclose(fPtr);
    	movefile(titletime,"./gacha_gacha");
		deletefile(titletime);	
    }
	for(int i = 0 ; i <10 ; i++)
		strcpy(tempresult[i], "");
}
```
### Question D
In that game, to do gacha items we have to use a medium of exchange called primogems. One time gacha item will spend primogems as many as 160 primogems. Because Mas Refadi wants the results of the gacha simulation looks like a lot, then in the program, primogems are defined at the beginning as 79000 primogems. Every time gacha, there are 2 properties that will be retrieved from the database, namely name and rarity. Then output the gacha results into a file (.txt) with gacha result format `{gacha-number}_[item-type]_{rarity}_{name}_{remaining-primogems}` The program will always gacha until the primogems run out.

So in that case the gacha is depends on the primogems. Each gacha also will deliver the rarity and the item name from each items has been obtained. From the source code in question before we made a fuction to gacha weapons and characters. Thus, we need to add function for rarity and item names. the code can be written as:

```c
const char* raritychr(char heroname[50]) {
	FILE *fp;
	char buffer[1024];
	struct json_object *parsed_json;
	struct json_object *rarity;
	size_t n_friends;

	size_t i;	
	char tmp[40];
	fp = fopen(heroname,"r");
	fread(buffer, 1024, 1, fp);
	fclose(fp);
	for (int i = 30 ; i <2000;i++){
		if ( buffer[i] == 'e' && buffer[i+1] == 'l' && buffer[i+2] == 'e'&&buffer[i+3] == 'm') { 
			buffer[i-2] = '}';
			buffer[i-1] = '\n';
			break;
			}
	}

	parsed_json = json_tokener_parse(buffer);
	json_object_object_get_ex(parsed_json, "rarity", &rarity);
//	return "aaaa";
	return json_object_get_string(rarity);
}

const char* getnamechr(char heroname[30]) {
	FILE *fp;
	char buffer[1024];
	struct json_object *parsed_json;
	struct json_object *name;
	size_t n_friends;

	size_t i;	

	fp = fopen(heroname,"r");
	fread(buffer, 1024, 1, fp);
	fclose(fp);
	for (int i = 30 ; i <2000;i++){
		if ( buffer[i] == 'e' && buffer[i+1] == 'l' && buffer[i+2] == 'e'&&buffer[i+3] == 'm') { 
			buffer[i-2] = '}';
			buffer[i-1] = '\n';
			break;
			}
	}

	parsed_json = json_tokener_parse(buffer);
	json_object_object_get_ex(parsed_json, "name", &name);
	return json_object_get_string(name);	
}

```

Lastly, remember each gach isn't free we must pay the gacha using prismogems we have therefore the code to do that we need to decrement the gems for each gacha cost. 

```c
waepongenerate();
		int gachaxx = 0;
		int money = 79000;
		int foldercheck = 1;
		int temp;
	  	while (1){
	  		if (money <0){ 
			  printf("Not enough amount of balance!!");
			  return 0;
			}
			printf("How many times do you want to gacha ?\n");
			
		  	scanf("%d", &temp);
			if (temp == 0) 
				return 0;
		  	printf("Fantastic, You got !\n");
			for (int i = 0; i < temp ; i++){
				money = money - 160;
		  		if(temp%2==1)
					gachawangychr(money, gachaxx);
				else	
					gachawangy(money, gachaxx);
				gachaxx++;
				if (gachaxx %10 == 0){				
					store(gachaxx, foldercheck-1);
					sleep(1);
					if (foldercheck % 9 == 0){
						foldercheck = storefolder(foldercheck*10);
					}
					printf("++>>%d",foldercheck ) ;
					foldercheck++;
				}

```


### Question E
The process for gacha items will start to coincide with the anniversary Mas Refadi's first time playing Bengshin Impek was on March 30 at 04:44. Then so that the results of the gacha are not seen by their boarding friends, then 3 hours after the anniversary, all the contents in the gacha_gacha folder will be zipped with the name not_safe_for_wibu with the password "satuduatiga", then all folders will be delete so that it only leaves the file (.zip)

To solve that we make a function namely time counter. reason behind that we could zip the file from the result of total gacha before at spesific time and lock it using a password.

```c
void timecounter(){
	pid_t child_id;
	child_id = fork();	
	char arr[80];
    time_t cek = time(NULL);
    char timetogo[10];
    struct tm timenow = *localtime(&cek);
    strftime(arr, sizeof(arr)-1, "%d-%m_%H:%M", &timenow);
    strcpy(timetogo, "30-03_");
	int jam = 4;
	char tmp[3];
	sprintf(tmp, "%02d", (jam+24-14)%24);
	strcat(timetogo, tmp);
	strcat(timetogo, ":44");
	 
	if (child_id < 0) {
		exit(EXIT_FAILURE);
	}

	if (child_id == 0) {
	} 
	
	else {
	while(strcmp(arr,timetogo)!=0) {
		cek = time(NULL);
		timenow = *localtime(&cek);
		strftime(arr, sizeof(arr)-1, "%d-%m_%H:%M", &timenow);
		sleep(10);
	}
   	char *argv2[] = {"zip","-r","not_safe_for_wibu.zip","-P", "satuduatiga", "gacha_gacha", NULL};
	execv("/bin/zip", argv2);   
	}
};
```
## REPORT NO.2 
### Question A 

The first thing the program needs to do is to trace the zip. given into the folder "/home/[user]/shift2/drakor". Because of Japrun's superiors teledor, in the zip can contain folders that are not important, then The program must be able to distinguish files and folders so that it can process the files that it should be done and delete unneeded folders.

```c
if(unzip == 0){
      char *argv[] = {"unzip", "drakor.zip", "*.png", "-d", "shift 2/drakor", NULL};
      execv("/bin/unzip", argv);
    }
```

We do the spawning process, in the child process we use the unzip command to unzip the drakor.zip files.
Here we use "*.png" to filter the files that is only the type png. 

### Question B

Korean drama posters need to be categorized according to their type, then the program must be
create folders for each type of Korean drama that is in the zip. Because you it is impossible to check one by one manuals, then the program must make the required folders correspond to the contents of the zip.

```c
void makeFolderSchool(){
  char *argv[] = {"mkdir", "-p", "shift 2/drakor/school", NULL};
    execv("/bin/mkdir", argv);
}

void makeFolderFantasy(){
  char *argv[] = {"mkdir", "-p", "shift 2/drakor/fantasy", NULL};
    execv("/bin/mkdir", argv);
}

```

For number 2, a fucntion is made for every category. The function will later be called in main. Here a child is made for every category. 

```c
int main(){

  pid_t try1 = fork();
  pid_t try2 = fork();
  pid_t try3 = fork();
  pid_t try4 = fork();
  pid_t try5 = fork();
  pid_t try6 = fork();
  pid_t try7 = fork();
  pid_t try8 = fork();

  if(try1 == 0){
    makeFolder();
  }

  if(try2 == 0){
    makeFolderFantasy();
  }

  if(try3 == 0){
    makeFolderSchool();
  }

  if(try4 == 0){
    makeFolderComedy();
  }

  if(try5 == 0){
    makeFolderRomance();
  }

  if(try6 == 0){
    makeFolderThriller();
  }

  if(try7 == 0){
    makeFolderAction();
  }

  if(try8 == 0){
    makeFolderHorror();
  }
  
  int stat;
  while(wait(&stat) > 0){
    pid_t unzip = fork();

    if(unzip == 0){
      char *argv[] = {"unzip", "drakor.zip", "*.png", "-d", "shift 2/drakor", NULL};
      execv("/bin/unzip", argv);
    }

    if (unzip < 0) {
      exit(EXIT_FAILURE); 
    }
  }

    return 0;
}
```

## REPORT NO.3 
### Question A
So in the first problem, we are asked to create a "darat" and "air" directory in modules with a 3-second pause. we use cid1&cid2 to make "darat" and "air" as below.
![img1](https://cdn.discordapp.com/attachments/651418861496696833/957631103596896256/unknown.png)

### Question B
for this time we use cid3 and fork the same as before using exec "unzip" and put it in "home/dft/module2/" as shown below. 
![img2](https://cdn.discordapp.com/attachments/651418861496696833/957632663739576400/unknown.png)

### Question C
In part C we still use fork and exec, namely exec "find" and followed by "cp" which means copy and paste from animal and entered according to the "darat" and "air" directories
![img3](https://cdn.discordapp.com/attachments/651418861496696833/957633525664841768/unknown.png)

### Question D
This time it's also the same using fork and exec to remove "bird" from the "darat" directory and here we add "rm" to remove it and still use "iname" to search for the word "bird"
![img4](https://cdn.discordapp.com/attachments/651418861496696833/957634998121402448/unknown.png)

### Question E
And for the last one, we are told to make a list from the water directory. So here we use string operators like strcmp and strcat, as below
![img5](https://cdn.discordapp.com/attachments/651418861496696833/957635313784721478/unknown.png)

Below are a few screenshots that were saved before my VM went CRAZY. sorry if only a little and what it is. it's all because of my VM error and I made 2 new VMs :) 

![img6](https://cdn.discordapp.com/attachments/651418861496696833/957637175363637268/unknown.png)
![img7](https://cdn.discordapp.com/attachments/651418861496696833/957637851774873731/unknown.png)
![img8](https://cdn.discordapp.com/attachments/651418861496696833/957639469324988416/unknown.png)
