#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <wait.h>
#include <time.h>
#include<dirent.h>
#include<json-c/json.h>

char weapon[135][70];
char characters[50][25];
char tempresult[50][70];
char txtname[10][50];
void directorygenerator();
void folderdownload();
void unzipdownloader();
void timecounter();

void makefolder(char index[20]){
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id < 0) {
    	exit(EXIT_FAILURE); 
    }
    if (child_id == 0) {
		char *argv[6] = {"mkdir",index,NULL};
		execv("/bin/mkdir", argv); 
	} 
	else {
    	while((wait(&status)) > 0);
		timecounter();
    	return;
    }
}

void makefile(char index[20]){
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id < 0) {
    	exit(EXIT_FAILURE); 
    }
    if (child_id == 0) {

		char *argv[6] = {"mkdir",index,NULL};
		execv("/bin/mkdir", argv); 
	} 
	else {
    	while((wait(&status)) > 0);
    	return;
    }
}

void movefile(char filename[15], char path[30]) {
    pid_t child_id;
    int status;

    child_id = fork();

    if (child_id < 0) {
      exit(EXIT_FAILURE); 
     }

     if (child_id == 0) {
       char *argv[6] = {"cp", filename, path, NULL};
       

       execv("/bin/cp", argv);
      } else {
        while((wait(&status)) > 0);
        return;       
      }
}

void deletefile(char filename[30]) {
    pid_t child_id;
    int status;

    child_id = fork();

    if (child_id < 0) {
      exit(EXIT_FAILURE); 
     }

     if (child_id == 0) {
	char *argv1[] = {"rm", "-r", filename, NULL};
	execv("/bin/rm", argv1);
      } else {
        while((wait(&status)) > 0);
        return;       
      }
}
	
void chargenerate(){
	DIR *d;
	struct dirent *dir;
	d = opendir("./characters/");
	if (d){
	int count =0;
	while ((dir = readdir(d)) != NULL){
		if (strlen(dir->d_name) > 5){
			strcpy(characters[count], dir->d_name);
			printf(">> %d.char %s\n", count,characters[count]);
			count++;
			}
		}
	closedir(d);
	}
}

void waepongenerate(){
	DIR *d;
	struct dirent *dir;
	d = opendir("./weapon/");
	if (d){
	int count =0;
	while ((dir = readdir(d)) != NULL){
		if (strlen(dir->d_name) > 5){
			strcpy(weapon[count], dir->d_name);
			printf(">> %d.wep %s\n", count, weapon[count]);
			count++;
			}
		}
	closedir(d);
	}
	chargenerate();
}

const char* rarity(char heroname[70]) {
	FILE *fp;
	char buffer[1024];
	struct json_object *parsed_json;
	struct json_object *rarity;
	size_t n_friends;

	size_t i;	
	char tmp[40];
	fp = fopen(heroname,"r");
	fread(buffer, 1024, 1, fp);
	fclose(fp);
	for (int i = 30 ; i <2000;i++){
		if ( buffer[i] == 'b' && buffer[i+1] == 'a' && buffer[i+2] == 's'&&buffer[i+3] == 'e') { 
			buffer[i-2] = '}';
			buffer[i-1] = '\n';
			break;
			}
	}

	parsed_json = json_tokener_parse(buffer);
	json_object_object_get_ex(parsed_json, "rarity", &rarity);
	return json_object_get_string(rarity);
}

const char* getname(char heroname[70]) {
	FILE *fp;
	char buffer[1024];
	struct json_object *parsed_json;
	struct json_object *name;
	size_t n_friends;

	size_t i;	

	fp = fopen(heroname,"r");
	fread(buffer, 1024, 1, fp);
	fclose(fp);
	for (int i = 30 ; i <2000;i++){
		if ( buffer[i] == 'b' && buffer[i+1] == 'a' && buffer[i+2] == 's'&&buffer[i+3] == 'e') { 
			buffer[i-2] = '}';
			buffer[i-1] = '\n';
			break;
			}
	}

	parsed_json = json_tokener_parse(buffer);
	json_object_object_get_ex(parsed_json, "name", &name);
	return json_object_get_string(name);	
}

const char* raritychr(char heroname[50]) {
	FILE *fp;
	char buffer[1024];
	struct json_object *parsed_json;
	struct json_object *rarity;
	size_t n_friends;

	size_t i;	
	char tmp[40];
	fp = fopen(heroname,"r");
	fread(buffer, 1024, 1, fp);
	fclose(fp);
	for (int i = 30 ; i <2000;i++){
		if ( buffer[i] == 'e' && buffer[i+1] == 'l' && buffer[i+2] == 'e'&&buffer[i+3] == 'm') { 
			buffer[i-2] = '}';
			buffer[i-1] = '\n';
			break;
			}
	}

	parsed_json = json_tokener_parse(buffer);
	json_object_object_get_ex(parsed_json, "rarity", &rarity);
//	return "aaaa";
	return json_object_get_string(rarity);
}

const char* getnamechr(char heroname[30]) {
	FILE *fp;
	char buffer[1024];
	struct json_object *parsed_json;
	struct json_object *name;
	size_t n_friends;

	size_t i;	

	fp = fopen(heroname,"r");
	fread(buffer, 1024, 1, fp);
	fclose(fp);
	for (int i = 30 ; i <2000;i++){
		if ( buffer[i] == 'e' && buffer[i+1] == 'l' && buffer[i+2] == 'e'&&buffer[i+3] == 'm') { 
			buffer[i-2] = '}';
			buffer[i-1] = '\n';
			break;
			}
	}

	parsed_json = json_tokener_parse(buffer);
	json_object_object_get_ex(parsed_json, "name", &name);
	return json_object_get_string(name);	
}
void gachawangy(int money, int gachaxx){
	char temp[10];
	char hero[70];
	int rndgnet = (time(0)+rand())%128;
	srand(gachaxx);		
	strcpy(hero, "./weapon/");
	strcat(hero, weapon[rndgnet]);
	sprintf(tempresult[gachaxx%10], "%d", gachaxx+1);
	strcat(tempresult[gachaxx%10], "_Waepon_");
	strcat(tempresult[gachaxx%10], rarity(hero));
	strcat(tempresult[gachaxx%10], "_");
	strcat(tempresult[gachaxx%10], getname(hero));
	strcat(tempresult[gachaxx%10], "_");
	sprintf(temp, "%d", money);
	strcat(tempresult[gachaxx%10], temp);
	printf(">>> %s\n", tempresult[gachaxx%10]);
}

void gachawangychr(int money, int gachaxx){
	char temp[10];
	char name[50];
	char hero[50];
	int rndgnet = (time(0)+rand())%47;
	srand(gachaxx);
	strcpy(hero, "./characters/");
	strcat(hero, characters[rndgnet]);
	sprintf(tempresult[gachaxx%10], "%d", gachaxx+1);
	strcat(tempresult[gachaxx%10], "_characters_");
	strcat(tempresult[gachaxx%10], raritychr(hero));
	strcat(tempresult[gachaxx%10], "_");
	strcat(tempresult[gachaxx%10], getnamechr(hero));
	strcat(tempresult[gachaxx%10], "_");
	sprintf(temp, "%d", money);
	strcat(tempresult[gachaxx%10], temp);
	printf(">>> %s\n", tempresult[gachaxx%10]);
}

void store(int gacha, int foldercount){
	printf(">>aa");
	time_t t;
    t = time(NULL);
    struct tm tm;
    tm = *localtime(&t);
	char titletime[20];
	char tmp[15];
	sprintf(titletime, "%02d", (tm.tm_hour+14)%24);
	strcat(titletime, ":");
	sprintf(tmp, "%02d", tm.tm_min);
	strcat(titletime, tmp);
	strcat(titletime, ":");
	sprintf(tmp, "%02d", tm.tm_sec);
	strcat(titletime, tmp);
	strcat(titletime, "_gacha_");
	sprintf(tmp, "%d", gacha);
	strcat(titletime, tmp);
	strcpy(txtname[foldercount],"./gacha_gacha/");
	strcat(txtname[foldercount],titletime);
    
    FILE * fPtr;
    fPtr = fopen(titletime, "w");
    if(fPtr == NULL){
        printf("Unable to create file.\n");
    }
    else{
    	for(int i = 0 ; i <10 ; i++){
    		fputs(tempresult[i],fPtr);
    		fputs("\n",fPtr);
    	}
		
    	fclose(fPtr);
    	movefile(titletime,"./gacha_gacha");
		deletefile(titletime);	
    }
	for(int i = 0 ; i <10 ; i++)
		strcpy(tempresult[i], "");
}

void directorygenerator(){
	pid_t child_id;
  	child_id = fork();
  	if (child_id < 0) {
    	exit(EXIT_FAILURE);
  	}
	if (child_id != 0) {
    	folderdownload();
  	}
  	else {
		char *argv[][6] = {{"mkdir","gacha_gacha",NULL},
			{"mkdir","characters",NULL},
			{"mkdir","weapon",NULL}
		};

		for(int anjg=0; anjg<3;anjg++){
		    if(fork()==0) continue;
		    sleep(10);
		    execv("/bin/mkdir", argv[anjg]); 
		}
  	}
}

int storefolder(int gacha){
	char name[40];
	char path[70];
	char tmp[10];
	sprintf(tmp, "%d", gacha);
	strcpy(name,"./gacha_gacha/total_gacha_");
	strcat(name,tmp);
	//gacha folder 
	makefolder(name);
	printf(">>>>  %s",name);
	
	for(int i = 0; i<9; i++){
	strcpy(path, "./gacha_gacha/");
	strcpy(path,txtname[i]);

	printf(">>store folder to %s ->> %s\n",path,txtname[i]);
		movefile(path,name);
		deletefile(path);	
	}
return (gacha/10);
}

void folderdownload(){
	pid_t child_id;
 	child_id = fork();
  	if (child_id < 0) {
		exit(EXIT_FAILURE);
	}
if (child_id == 0) {
		unzipdownloader();
	} 
  	else {
    	char *argv1[][10] = {
			{ "wget","-q","--no-check-certificate","https://drive.google.com/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download", "-O","weapon.zip",NULL},
			{ "wget","-q","--no-check-certificate","https://drive.google.com/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download", "-O","characters.zip",NULL},
		};
		for(int i=0;i<2;i++){
			if(fork()==0) continue;
			execv("/bin/wget", argv1[i]);
		}
		sleep(15);i
  	}
}
void unzipdownloader(){
	pid_t child_id;
	child_id = fork();
	
	if (child_id < 0) {
		exit(EXIT_FAILURE);
	}
if (child_id == 0) {
	} 
	else {
		char *argv3[][10]= {
			{"unzip","-j","characters.zip","-d","./characters",NULL},
			{"unzip","-j","weapon.zip","-d","./weapon",NULL}
		};
	for(int anjg=0; anjg<3;anjg++){
			if(fork()==0) continue;
			sleep(10);
			execv("/bin/unzip", argv3[anjg]); 
		}
	}
}
void timecounter(){
	pid_t child_id;
	child_id = fork();	
	char arr[80];
    time_t cek = time(NULL);
    char timetogo[10];
    struct tm timenow = *localtime(&cek);
    strftime(arr, sizeof(arr)-1, "%d-%m_%H:%M", &timenow);
    //generate time eksekuci
    strcpy(timetogo, "30-03_");
	int jam = 4;
	char tmp[3];
	sprintf(tmp, "%02d", (jam+24-14)%24);
	strcat(timetogo, tmp);
	strcat(timetogo, ":44");
	 
	if (child_id < 0) {
		exit(EXIT_FAILURE);
	}

	if (child_id == 0) {
	} 
	
	else {
	while(strcmp(arr,timetogo)!=0) {
		cek = time(NULL);
		timenow = *localtime(&cek);
		strftime(arr, sizeof(arr)-1, "%d-%m_%H:%M", &timenow);
		//printf("%s\n",arr);
		sleep(10);
	}
   	char *argv2[] = {"zip","-r","not_safe_for_wibu.zip","-P", "satuduatiga", "gacha_gacha", NULL};
	execv("/bin/zip", argv2);   
	}
};

int main() {
	pid_t child_id;
	int status;
	child_id = fork();
	if (child_id < 0) {
		exit(EXIT_FAILURE); 
		// Jika gagal membuat proses baru, program akan berhenti
  	}

	//preprocesing

	if (child_id == 0) {
		//Ini bukan orangtua boleh di lawan
		directorygenerator();
  	}
  	else {
  		//sleep(15);
		waepongenerate();
		int gachaxx = 0;
		int money = 79000;
		int foldercheck = 1;
		int temp;
	  	while (1){
	  		if (money <0){ 
			  printf("Not enough amount of balance!!");
			  return 0;
			}
			printf("How many times do you want to gacha ?\n");
			
		  	scanf("%d", &temp);
			if (temp == 0) 
				return 0;
		  	printf("Fantastic, You got !\n");
			for (int i = 0; i < temp ; i++){
				money = money - 160;
		  		if(temp%2==1)
					gachawangychr(money, gachaxx);
				else	
					gachawangy(money, gachaxx);
				gachaxx++;
				if (gachaxx %10 == 0){				
					store(gachaxx, foldercheck-1);
					sleep(1);
					if (foldercheck % 9 == 0){
						foldercheck = storefolder(foldercheck*10);
					}
					printf("++>>%d",foldercheck ) ;
					foldercheck++;
				}
			}
		}
	}
	return 0;
}       


