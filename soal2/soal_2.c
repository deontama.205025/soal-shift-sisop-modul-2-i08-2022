#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <stdio.h>
#include <wait.h>


void makeFolder(){
    char *argv[] = {"mkdir", "-p", "shift 2/drakor", NULL};
    execv("/bin/mkdir", argv);
}

void makeFolderSchool(){
  char *argv[] = {"mkdir", "-p", "shift 2/drakor/school", NULL};
    execv("/bin/mkdir", argv);
}

void makeFolderFantasy(){
  char *argv[] = {"mkdir", "-p", "shift 2/drakor/fantasy", NULL};
    execv("/bin/mkdir", argv);
}

void makeFolderComedy(){
  char *argv[] = {"mkdir", "-p", "shift 2/drakor/comedy", NULL};
    execv("/bin/mkdir", argv);
}

void makeFolderRomance(){
  char *argv[] = {"mkdir", "-p", "shift 2/drakor/romance", NULL};
    execv("/bin/mkdir", argv);
}

void makeFolderThriller(){
  char *argv[] = {"mkdir", "-p", "shift 2/drakor/thriller", NULL};
    execv("/bin/mkdir", argv);
}

void makeFolderHorror(){
  char *argv[] = {"mkdir", "-p", "shift 2/drakor/horror", NULL};
    execv("/bin/mkdir", argv);
}

void makeFolderAction(){
  char *argv[] = {"mkdir", "-p", "shift 2/drakor/Action", NULL};
    execv("/bin/mkdir", argv);
}



int main(){

  pid_t try1 = fork();
  pid_t try2 = fork();
  pid_t try3 = fork();
  pid_t try4 = fork();
  pid_t try5 = fork();
  pid_t try6 = fork();
  pid_t try7 = fork();
  pid_t try8 = fork();

  if(try1 == 0){
    makeFolder();
  }

  if(try2 == 0){
    makeFolderFantasy();
  }

  if(try3 == 0){
    makeFolderSchool();
  }

  if(try4 == 0){
    makeFolderComedy();
  }

  if(try5 == 0){
    makeFolderRomance();
  }

  if(try6 == 0){
    makeFolderThriller();
  }

  if(try7 == 0){
    makeFolderAction();
  }

  if(try8 == 0){
    makeFolderHorror();
  }
  
  int stat;
  while(wait(&stat) > 0){
    pid_t unzip = fork();

    if(unzip == 0){
      char *argv[] = {"unzip", "drakor.zip", "*.png", "-d", "shift 2/drakor", NULL};
      execv("/bin/unzip", argv);
    }

    if (unzip < 0) {
      exit(EXIT_FAILURE); 
    }
  }

    return 0;
}
