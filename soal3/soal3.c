#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <dirent.h>
#include <fcntl.h>
#include <errno.h>
#include <wait.h>
#include <pwd.h>
#include <sys/types.h>
#include <sys/unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>

int main(void){
  pid_t cid1, cid2, cid3, cid4, cid5, cid6, cid7;
  int status;

  cid1 = fork();
  if(cid1 < 0){
      exit(EXIT_FAILURE);
  }

  if(cid1 == 0){
    char *argv[] = {"mkdir", "/home/dft/modul2/darat",  NULL};
    execv("/bin/mkdir", argv);
  } else{
    while((wait(&status)) > 0);
    cid2 = fork();

    if(cid2 < 0){
      exit(EXIT_FAILURE);
    }

    if(cid2 == 0){
      sleep(3);
      char *argv[] = {"mkdir", "/home/dft/modul2/air", NULL};
      execv("/bin/mkdir", argv);
    } else{
      while((wait(&status)) > 0);
      cid3 = fork();

      if(cid3 < 0){
          exit(EXIT_FAILURE);
      }

      if(cid3 == 0){
          char *argv[] = {"unzip", "-q", "animal.zip", "-d", "/home/dft/modul2/", NULL};
          execv("/usr/bin/unzip", argv);
      } else{
        while((wait(&status)) > 0);
        cid4 = fork();

        if(cid4 < 0){
        exit(EXIT_FAILURE);
        }

        if(cid4 == 0){
            char *argv[] = {"find", "./modul2/animal", "-iname", "*air*", "-exec", "cp", "{}", "./modul2/air/", ";", NULL};
            execv("/usr/bin/find", argv);
        } else{
            while((wait(&status)) > 0);
            cid5 = fork();

          if(cid5 < 0){
            exit(EXIT_FAILURE);
          }

          if(cid5 == 0){
            char *argv[] = {"find", "./modul2/animal", "-iname", "*darat*", "-exec", "cp", "{}", "./modul2/darat/", ";", NULL};
            execv("/usr/bin/find", argv);
          } else {
            while(wait(&status) > 0);
            pid_t child1_id = fork();

            if(child1_id < 0) exit(EXIT_FAILURE);

            if(child1_id == 0) {
                char *argv[] = {"rm", "-r", "./modul2/animal", NULL};
                execv("/usr/bin/rm", argv);
            }
            cid6 = fork();

            if(cid6 < 0){
              exit(EXIT_FAILURE);
            }

            if(cid6 == 0){
              char *argv[] = {"find", "./modul2/darat", "-iname", "*bird*", "-exec", "rm", "{}", ";", NULL};
              execv("/usr/bin/find", argv);
            } else{
              while(wait(&status) > 0);
              cid7 = fork();

	      if(cid7 < 0){
		exit(EXIT_FAILURE);
	      }

              if(cid7 == 0){
                DIR *dp;
                struct dirent *ep;
                char filename[300], forstat[300];
                FILE *fptr;
                struct stat fs;
                  
                fptr = fopen("modul2/air/list.txt", "w");

                dp = opendir("modul2/air");

                if (dp != NULL)
                {
                  while ((ep = readdir (dp))) {
                    if(strcmp(ep->d_name, ".") == 0 || strcmp(ep->d_name, "..") == 0 || strcmp(ep->d_name, "list.txt") == 0) continue;
                    sprintf(forstat, "modul2/air/%s", ep->d_name);
                    stat(forstat, &fs);
                    sprintf(filename, "%s_", getpwuid(fs.st_uid)->pw_name);
                    if( fs.st_mode & S_IRUSR ) strcat(filename, "r");
                    if( fs.st_mode & S_IWUSR ) strcat(filename, "w");
                    if( fs.st_mode & S_IXUSR ) strcat(filename, "x");
                    strcat(filename, "_");
                    strcat(filename, ep->d_name);
                    fprintf(fptr, "%s\n", filename);
                  }
                  (void) closedir (dp);
                } else printf("Couldn't open the directory");
                  
                fclose(fptr);
              } else{
              while(wait(&status) > 0);
              }
            }
          }
        }
      }
    }
  }
}
